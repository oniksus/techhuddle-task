<?php

namespace App\Core;

use App\Http\Request;
use App\Http\Response;

class Router
{
    /**
     * @var Request
     */
    private $request;

    private $routes = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Match request to route
     *
     * @return bool
     */
    public function match() : bool
    {
        $controllerData = $this->_getRouteControllerData();

        return !empty($controllerData);
    }

    public function execute()
    {
        $controllerData = $this->_getRouteControllerData();
        if (empty($controllerData)) {
            throwException('Cannot match route to controller');
        }

        if (!isset($controllerData['controller'])
            || !isset($controllerData['action'])) {
            throwException('Invalid controller configuration for route %s', $this->request->getUri());
        }

        $controllerInstance = getClass($controllerData['controller']);
        $action = $controllerData['action'];

        $response = $controllerInstance->$action();

        if (is_string($response)) {
            return new Response(200, $response);
        }

        return $response;
    }

    private function _getRouteControllerData() : array
    {
        if ($this->request->isPost()) {
            $type = Request::TYPE_POST;
        } else {
            $type = Request::TYPE_GET;
        }

        $controllerData = [];
        if (isset($this->routes[$type])) {
            foreach ($this->routes[$type] as $route => $controller) {
                if ($this->_matchesRoute($route)) {
                    $controllerData = $controller;
                    break;
                }
            }
        }

        return $controllerData;
    }

    private function _matchesRoute(string $route) : bool
    {
        $uri = $this->request->getUri();
        return $route == $uri;
    }

    /**
     * @param string $path
     * @param string $handler
     * @return Router
     * @throws \Exception
     */
    public function get(string $path, string $handler) : Router
    {
        $this->routes[Request::TYPE_GET][$path] = $this->_parseHandler($handler);

        return $this;
    }

    /**
     * @param string $path
     * @param string $handler
     * @return Router
     * @throws \Exception
     */
    public function post(string $path, string $handler) : Router
    {
        $this->routes[Request::TYPE_POST][$path] = $this->_parseHandler($handler);
        return $this;
    }

    /**
     * @param string $handler
     * @return array
     * @throws \Exception
     */
    public function _parseHandler(string $handler) : array
    {
        $action = null;
        $controller = null;

        $parts = explode('@', $handler);

        if (count($parts) === 1) {
            $controller = 'App\\Http\\Controllers\\' . $handler;
            $action = 'indexAction';
        } else if (count($parts) === 2) {
            $controller = 'App\\Http\\Controllers\\' . array_shift($parts);
            $action = sprintf('%sAction', array_shift($parts));
        } else {
            throwException('"%s" is  a invalid controller definition, please use {controller}@{action}', $handler);
        }

        if (!class_exists($controller)) {
            throwException('Invalid controller class %s', $controller);
        }

        if (!method_exists($controller, $action)) {
            throwException('Invalid controller action %s for %s', $action, $controller);
        }

        return [
            'controller' => $controller,
            'action' => $action,
        ];
    }
}