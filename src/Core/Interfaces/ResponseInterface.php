<?php

namespace App\Core\Interfaces;


interface ResponseInterface
{
    public function send() : ResponseInterface;
}