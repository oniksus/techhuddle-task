<?php
namespace App\Core\Interfaces;

use App\Core\ServiceContainer;

interface FactoryInterface
{
    public function build(ServiceContainer $container);
}