<?php


namespace App\Core;


class Session
{
    public function __construct()
    {
        session_start();
    }

    public function set(string $key, string $value) : Session
    {
        $_SESSION[$key] = $value;

        return $this;
    }

    public function get(string $key) : ?string
    {
        return isset($_SESSION[$key]) ? (string) $_SESSION[$key] : null;
    }

    public function remove(string $key) : bool
    {
        if(isset($_SESSION[$key])){
            unset($_SESSION[$key]);
            return true;
        }

        return false;
    }

    public function destroy() : bool
    {
        return session_destroy();
    }
}