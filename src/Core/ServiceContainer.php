<?php

namespace App\Core;

use App\Core\Interfaces\FactoryInterface;
use App\Http\Request;

class ServiceContainer
{
    private $singletons;
    private $factories;

    public function __construct()
    {
        $this->singletons = [
            Request::class => new Request(),
            Session::class => new Session()
        ];
    }

    public function get(string $class)
    {
        if (isset($this->singletons[$class])) {
            return $this->singletons[$class];
        }

        return $this->_get($class);
    }

    /**
     * Simple class getter using reflection to resolve __construct parameters
     *
     * @param string $class
     * @return object
     * @throws \ReflectionException
     */
    public function _get(string $class)
    {
        if (!class_exists($class)) {
            throwException('Cannot find class %s', $class);
        }

        if (isset($this->factories[$class])) {
            // Used this initially!!!
            $factoryClass = $this->factories[$class];
            /** @var FactoryInterface $factory */
            $factory = new $factoryClass();
            $object = $factory->build($this);

        } else {
            $class = new \ReflectionClass($class);
            $constructor = $class->getConstructor();

            $params = $constructor->getParameters();
            if (!empty($params)) {
                $instances = [];
                foreach ($params AS $param) {
                    $instances[] = $this->get($param->getClass()->name);
                }
                $object = $class->newInstanceArgs($instances);
            } else {
                $object = new $class();
            }
        }

        return $object;
    }
}