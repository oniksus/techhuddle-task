<?php

namespace App\Model;

use App\Core\Session;
use App\Exception\InvalidBoard;
use App\Exception\InvalidCoordinate;
use App\Exception\InvalidHit;
use App\Exception\InvalidShip;
use App\Http\Request;
use App\Model\Ships\Battleship;
use App\Model\Ships\Destroyer;
use App\Model\Ships\Ship;
use phpDocumentor\Reflection\Types\Boolean;

class BattleshipGame
{
    const DRAWABLE_LEFT  = 'L';
    const DRAWABLE_RIGHT = 'R';
    const DRAWABLE_UP    = 'U';
    const DRAWABLE_DOWN  = 'D';

    const GAME_DATA_SESSION_KEY = 'game-field-data';

    const LINE_PREFIXES = ['A','B','C','D','E','F','G','H','I','J'];

    /**
     * @var array
     */
    private $field;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var Request
     */
    private $request;

    private $gameIsComplete = false;

    /**
     * @var Ship[]
     */
    private $ships = [];

    private $data = [];

    public function __construct(Request $request, Session $session)
    {
        $this->session = $session;
        $this->request = $request;

        $data = $this->getSavedGameState();
        if (!empty($data)) {
            $this->setGameData($data);
        } else {
            $this->initGame();
        }
    }

    /**
     * Create a new game instance and generate ship coordinates using random numbers
     *
     * @return BattleshipGame
     * @throws InvalidBoard
     * @throws InvalidCoordinate
     * @throws InvalidHit
     * @throws InvalidShip
     */
    public function initGame() : BattleshipGame
    {
        $this->resetGame();
        $ships = [];
        $takenCoordinates = [];
        $this->ships[0] = $this->createBattleship([]);
        foreach ($this->ships[0]->getShipCoordinates() as $coordinate) {
            $ships[0][] = $coordinate;
            $takenCoordinates[$coordinate] = $coordinate;
        }

        $this->ships[1] = $this->createDestroyer($takenCoordinates);
        foreach ($this->ships[1]->getShipCoordinates() as $coordinate) {
            $ships[1][] = $coordinate;
            $takenCoordinates[$coordinate] = $coordinate;
        }

        $this->ships[2] = $this->createDestroyer($takenCoordinates);
        foreach ($this->ships[2]->getShipCoordinates() as $coordinate) {
            $ships[2][] = $coordinate;
        }

        $data = [
            'ships' => $ships,
        ];

        return $this->setGameData($data);
    }

    public function resetGame() : void
    {
        $this->field = $this->getEmptyField();
        $this->data = [];
        $this->session->set('show', '');
    }

    public function restartGame()
    {
        $this->resetGame();
        $this->session->set('message', '');
        $this->session->set('attempts', 0);
        $this->session->set('show', '');
    }

    /**
     * Transform submitted data to coordinates
     *
     * @param $coordinates
     * @return array
     * @throws InvalidCoordinate
     */
    public function parseFireCoordinates($coordinates) : array
    {
        $firstLetter = mb_strtoupper(substr($coordinates,0,1));
        $keys = array_flip(static::LINE_PREFIXES);
        $y = isset($keys[$firstLetter]) ? $keys[$firstLetter] : -100;

        $x = substr($coordinates,1);
        if (!is_numeric($x) || $y < 0) {
            throw new InvalidCoordinate("Invalid X,Y ({$x}:{$y}) coordinates");
        } else {
            $x = $x - 1;
        }

        return [(int)$x, (int)$y];
    }

    public function isGameComplete() : bool
    {
        return $this->gameIsComplete;
    }

    public function getFiredShotsCount() : int
    {
        $attempts = $this->session->get('attempts') ? $this->session->get('attempts') : 0;

        return $attempts;
    }

    /**
     * Check if a ship is hit
     *
     * @param int $x
     * @param int $y
     * @return bool
     * @throws InvalidCoordinate
     */
    public function isHit(int $x, int $y) : bool
    {
        if (!isset($this->field[$y][$x])) {
            throw new InvalidCoordinate("Out of bord coordinates X,Y ({$x}:{$y})");
        }

        $isHit = false;
        // can also just use in_array($coordinates, $this->data['hits']
        foreach ($this->ships as $ship) {
            /** @var Ship $ship */
            if ($ship->isHit($x, $y)) {
                $coordinates = "{$x}:{$y}";
                if (!in_array($coordinates, $this->data['hits'])) {
                    // prevent hitting the same target 2 times
                    $isHit = true;
                }
                break;
            }
        }

        return $isHit;
    }

    public function registerHit(int $x, int $y) : void
    {
        $coordinates = "{$x}:{$y}";
        $this->field[$y][$x] = 2;
        if (!in_array($coordinates, $this->data['hits'])) {
            $this->data['hits'][] = $coordinates;
            if (count($this->data['hits']) == $this->getTotalShipSquares()) {
                $this->gameIsComplete = true;
            }
        }
    }

    public function saveGameState() : BattleshipGame
    {
        $this->session->set(static::GAME_DATA_SESSION_KEY, json_encode($this->data));

        return $this;
    }

    public function getSavedGameState() : ?array
    {
        $data = $this->session->get(static::GAME_DATA_SESSION_KEY);
        if (strlen($data) > 10) {
            $data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        } else {
            $data = null;
        }

        return $data;
    }

    /**
     * Validate the data, initialize the game, populate the field with ships
     *
     * @param array $data
     * @return BattleshipGame
     * @throws InvalidBoard
     * @throws InvalidCoordinate
     * @throws InvalidHit
     * @throws InvalidShip
     */
    private function setGameData(array $data): BattleshipGame
    {
        $this->resetGame();
        if (!isset($data['ships'])) {
            throw new InvalidBoard('Missing Ships Data');
        }

        $totalShipSquares = $this->getTotalShipSquares();

        $shipSquares = 0;

        // FLIP X and Y positions in the field so the coordinates match the standard XY access
        foreach ($data['ships'] as $ship) {
            foreach ($ship as $cord) {
                list($x, $y) = $this->getXYCoordinates($cord);
                if (isset($this->field[$y][$x]) && $this->field[$y][$x] == 1) {
                    throw new InvalidCoordinate(sprintf('Ship overlap at coordinate %s', $cord));
                }
                // register field cell as SHIP
                $this->field[$y][$x] = 1;
                $shipSquares++;
            }

            $this->ships[] = Ship::getInstanceFromCoordinates($ship);
        }

        if ($shipSquares != $totalShipSquares) {
            throw new InvalidBoard('Missing Ships Data');
        }

        $this->data['ships'] = $data['ships'];

        if (isset($data['hits']) && count($data['hits']) > 0) {
            // validate ship hits
            if (count($data['hits']) > $totalShipSquares) {
                throw new InvalidHit('Too many ship hits');
            }

            foreach ($data['hits'] as $hitCord) {
                list($x, $y) = $this->getXYCoordinates($hitCord);

                if (!isset($this->field[$y][$x]) || $this->field[$y][$x] == 0) {
                    throw new InvalidHit(sprintf('Invalid hit %s', $hitCord));
                } else {
                    // register field cell as HIT
                    $this->field[$y][$x] = 2;
                }
            }

            $this->data['hits'] = $data['hits'];
            if (count($this->data['hits']) == $this->getTotalShipSquares()) {
                $this->gameIsComplete = true;
            }
        }

        return $this;
    }

    /**
     * Render game area
     *
     * @return string
     */
    public function renderField() : string
    {
        if ($this->isGameComplete()) {
            $message = GameMessages::SUNK;
        } else {
            $message = $this->session->get('message');
        }
        $field = '';
        if ($message) {
            $field = ' ' . $message . PHP_EOL . PHP_EOL;
        }

        $show = $this->session->get('show') != '';

        $field .= '  1 2 3 4 5 6 7 8 9 10' . PHP_EOL;

        $linePrefixes = static::LINE_PREFIXES;


        foreach ($this->field as $y => $line) {
            $field .= $linePrefixes[$y];
            foreach ($line as $x => $state) {
                if ($this->isGameComplete()) {
                    if ($state == 0) {
                        $field .= ' .';
                    } else {
                        $field .= ' X';
                    }
                } else if ($show) {
                    if ($state == 1) {
                        $field .= ' X';
                    } else {
                        $field .= '  ';
                    }
                } else {
                    if ($state == 2) {
                        $field .= ' X';
                    } else {
                        $field .= ' .';
                    }
                }
            }
            $field .= PHP_EOL;
        }


        return $field;
    }

    private function createBattleship(array $alreadyTakenCoordinates): ?Ship
    {
        return $this->createShipOnRandomCoordinates('battleship', $alreadyTakenCoordinates);
    }

    private function createDestroyer(array $alreadyTakenCoordinates): ?Ship
    {
        return $this->createShipOnRandomCoordinates('destroyer', $alreadyTakenCoordinates);
    }

    private function createShipOnRandomCoordinates(string $type, array $alreadyTakenCoordinates): ?Ship
    {
        if ($type == 'battleship') {
            $length = Battleship::LENGTH;
            $class = Battleship::class;
        } else {
            $length = Destroyer::LENGTH;
            $class = Destroyer::class;
        }

        while (true) {
            $ship = null;
            $x1 = rand(0, 9);
            $y1 = rand(0, 9);

            $directions = $this->getDrawableDirections($x1,$y1,$length,$alreadyTakenCoordinates);

            $dirKey = rand(0, 3);
            if(!isset($directions[$dirKey])) {
                continue;
            }

            if ($directions[$dirKey] == static::DRAWABLE_LEFT) {
                $x2 = $x1 - ($length - 1);
                $y2 = $y1;
            } else if ($directions[$dirKey] == static::DRAWABLE_RIGHT) {
                $x2 = $x1 + ($length - 1);
                $y2 = $y1;
            } else if ($directions[$dirKey] == static::DRAWABLE_DOWN) {
                $x2 = $x1;
                $y2 = $y1 + ($length - 1);
            } else if ($directions[$dirKey] == static::DRAWABLE_UP) {
                $x2 = $x1;
                $y2 = $y1 - ($length - 1);
            } else {
                continue;
            }

            /** @var Ship $ship */
            $ship = new $class($x1, $y1, $x2, $y2);

            return $ship;
        }

        return null;
    }

    /**
     * Check directions in which a new ship can be drawn
     *
     * @param int $x1
     * @param int $y1
     * @param int $shipSize
     * @param array $alreadyTakenCoordinates
     * @return array
     */
    private function getDrawableDirections(int $x1, int $y1, int $shipSize, array $alreadyTakenCoordinates) : array
    {
        // first field is the x1,y1 coordinates
        $shipSize = $shipSize - 1;
        $canDraw = function (int $start,int $limit,bool $isXDirection) use ($alreadyTakenCoordinates, $x1, $y1) : bool {
            if ($isXDirection) {
                for($i=$start;$i<=$limit;$i++) {
                    if (in_array("{$i}:{$y1}", $alreadyTakenCoordinates)) {
                        return false;
                    }
                }
            } else {
                for($i=$start;$i<=$limit;$i++) {
                    if (in_array("{$x1}:{$i}", $alreadyTakenCoordinates)) {
                        return false;
                    }
                }
            }

            return true;
        };

        $directions = [];

        // check if x2 and y2 is in board and doesn't cross another ship
        if (($x1 + $shipSize) <= 9 && $canDraw($x1,($x1 + $shipSize),true)) {
            $directions[static::DRAWABLE_RIGHT] = static::DRAWABLE_RIGHT;
        }

        if (($x1 - $shipSize) >= 0 && $canDraw(($x1 - $shipSize),$x1,true)) {
            $directions[static::DRAWABLE_LEFT] = static::DRAWABLE_LEFT;
        }

        if (($y1 + $shipSize) <= 9 && $canDraw($y1,($y1 + $shipSize),false)) {
            $directions[static::DRAWABLE_DOWN] = static::DRAWABLE_DOWN;
        }

        if (($y1 - $shipSize) >= 0 && $canDraw(($y1 - $shipSize),$y1,false)) {
            $directions[static::DRAWABLE_UP] = static::DRAWABLE_UP;
        }
        $directions = array_values($directions);
        sort($directions); // sort for consistency when testing

        return $directions;
    }

    /**
     * Get X Y from standard coordinate format
     *
     * @param $coordinate
     * @return array
     * @throws InvalidCoordinate
     */
    private function getXYCoordinates($coordinate) : array
    {
        if (strpos($coordinate, ':') === false) {
            throw new InvalidCoordinate(sprintf('Invalid coordinate %s', $coordinate));
        }

        list($x, $y) = explode(':', $coordinate);

        $x = (int) $x;
        $y = (int) $y;

        if ($x >= 0 && $x <= 9 && $y >= 0 && $y <= 9) {
            return [$x, $y];
        } else {
            throw new InvalidCoordinate(sprintf('Invalid coordinate %s', $coordinate));
        }
    }

    private function getEmptyField() : array
    {
        return [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];
    }

    /**
     * Total number of ship squires on the field
     *
     * @return int
     */
    private function getTotalShipSquares() : int
    {
        return ((Destroyer::LENGTH * 2) + Battleship::LENGTH);
    }
}
