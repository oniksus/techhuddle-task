<?php


namespace App\Model;


class GameMessages
{
    const HIT = '**** Hit ***';
    const MISS = '**** Miss ***';
    const ERROR = '**** Error ***';
    const SUNK  = '**** Sunk ***';
}