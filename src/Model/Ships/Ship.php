<?php

namespace App\Model\Ships;

use App\Exception\InvalidShip;

abstract class Ship
{
    const LENGTH = 3;

    protected $shipCoordinates;
    protected $coordinates;

    /**
     * Ship constructor.
     *
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     * @throws InvalidShip
     */
    public function __construct(int $x1, int $y1, int $x2, int $y2)
    {
        if (static::LENGTH < 4) {
            throw new InvalidShip('Not a military ship');
        }

        if ($x1 > $x2) {
            $x = $x2;
            $x2 = $x1;
            $x1 = $x;
        }

        if ($y1 > $y2) {
            $y = $y2;
            $y2 = $y1;
            $y1 = $y;
        }

        if ($this->validateCoordinates($x1, $y1, $x2, $y2)) {
            $this->coordinates = [$x1, $y1, $x2, $y2];

            if ($x1==$x2) {
                for ($y=$y1;$y<=$y2;$y++) {
                    $coordinate = "{$x1}:{$y}";
                    $this->shipCoordinates["{$x1}:{$y}"] = $coordinate;
                }
            } else {
                for ($x=$x1;$x<=$x2;$x++) {
                    $coordinate = "{$x}:{$y1}";
                    $this->shipCoordinates[$coordinate] = $coordinate;
                }
            }
        } else {
            throw new InvalidShip('Cannot validate coordinates');
        }

        return;
    }

    public function getShipCoordinates() : array
    {
        return $this->shipCoordinates;
    }
    
    public function validateCoordinates(int $x1, int $y1, int $x2, int $y2) : bool
    {
        $isValid = false;
        if ($x1 == $x2 && (abs($y1-$y2)+1) == static::LENGTH) {
            $isValid = true;
        } else if ($y1 == $y2 && (abs($x1-$x2)+1) == static::LENGTH) {
            $isValid = true;
        }

        foreach ([$x1, $x2, $y1, $y2] as $cord) {
            if ($cord > 9 || $cord < 0) {
                $isValid = false;
                break;
            }
        }

        return $isValid;
    }

    /**
     * Is the XY coordinate inside the ship
     *
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function isHit(int $x, int $y) : bool
    {
        return isset($this->shipCoordinates["{$x}:{$y}"]);
    }

    /**
     * get ship instance form saved coordinates format
     *
     * @param array $coordinates
     * @return Ship
     * @throws InvalidShip
     */
    static function getInstanceFromCoordinates(array $coordinates) : Ship
    {
        $type = count($coordinates) == 5 ? 'battleship' : 'destroyer';
        list($x1, $y1) = array_map('intval', explode(':', array_shift($coordinates)));
        list($x2, $y2) = array_map('intval', explode(':', array_pop($coordinates)));

        if ($type == 'battleship') {
            return new Battleship($x1,$y1,$x2,$y2);
        } else {
            return new Destroyer($x1,$y1,$x2,$y2);
        }
    }
}