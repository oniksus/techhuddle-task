<?php


namespace App\Utils;

use App\Core\Session;

class TestSessionHelper extends Session
{
    public function __construct($data)
    {
        $_SESSION = $data;
    }
}