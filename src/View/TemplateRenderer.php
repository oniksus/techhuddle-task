<?php

namespace App\View;

use Exception;

class TemplateRenderer
{
    private $template;
    public $params = [];

    public function __construct(string $template, array $params)
    {
        $this->template = $template;
        $this->params = $params;
    }

    public function render() : string
    {
        ob_start();
        try {
            $path = dirname(SRC_ROOT_PATH) . DS . 'templates' . DS . $this->template.'.phtml';
            if (file_exists($path)) {
                include $path;
            } else {
                throwException('Invalid template %s', $this->template);
            }
        } catch (Exception $e) {
            ob_get_clean();
            throw $e;
        }

        $html = ob_get_clean();

        return $html;
    }

    public function get($key)
    {
        return isset($this->params[$key]) ? $this->params[$key] : null;
    }
}