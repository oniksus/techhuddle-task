<?php

use App\Core\ServiceContainer;

define('DS', DIRECTORY_SEPARATOR);
define('SRC_ROOT_PATH', dirname(__FILE__));
define('TEST_ROOT_DIR', dirname(__DIR__).'/tests/');
chdir(dirname(SRC_ROOT_PATH)); // set to project root

function throwException(string $message, ...$args) {
    throw new \Exception(vsprintf($message, $args));
}

/**
 * @param string $class
 * @throws Exception
 */
function customAutoloader(string $class): void {
    $nameSpaceMapping = [
        'App' => SRC_ROOT_PATH,
        'Tests' => TEST_ROOT_DIR,
    ];

    $rootNamespace = substr($class, 0, strpos($class, '\\'));
    if (!isset($nameSpaceMapping[$rootNamespace])) {
        throwException('Cannot load class: "%s"', $class);
    }

    $classPath = str_replace(['\\', '/', '\\\\'], DS, $class);
    $classPath = str_replace($rootNamespace, $nameSpaceMapping[$rootNamespace], $classPath);
    $classPath .= '.php';

    if (!file_exists($classPath)) {
        throwException('Cannot find file "%s" for class "%s"', $classPath, $class);
    }

    require_once $classPath;
}

spl_autoload_register('customAutoloader');

// provide very simple dependance injection
$container = new ServiceContainer();
function getClass(string $class) {
    global $container;
    return $container->get($class);
}