<?php

namespace App\Http;

use App\Core\Session;
use App\View\TemplateRenderer;

class BaseController
{
    public $params;

    /**
     * @var
     */
    protected $request;
    protected $session;

    public function __construct(Request $request, Session $session)
    {
        $this->request = $request;
        $this->session = $session;
    }

    public function render($template, $params) : string
    {
        $template = new TemplateRenderer($template, $params);

        return $template->render();
    }
}