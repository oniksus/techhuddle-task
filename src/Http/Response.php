<?php

namespace App\Http;

use App\Core\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    private $code;
    private $content;
    private $type;

    public function __construct(
        int $code,
        string $content = null,
        $type = 'text/html'
    ) {
        $this->code = $code;
        $this->content = $content;
        $this->type = $type;
    }

    public function send() : ResponseInterface
    {
        $this->sendHeaders();
        $this->sendContent();

        return $this;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            return $this;
        }

        header(sprintf("Content-Type: %s", $this->type));
        header(sprintf("HTTP/1.0 %s %s", $this->code, $this->getCodeMessage($this->code)));

        return $this;
    }

    public function sendContent()
    {
        echo $this->content;

        return $this;
    }

    public function getCodeMessage(int $code) : string
    {
        if ($code === 200) {
            $message = "OK";
        } else if ($code === 404) {
            $message = "Not Found";
        } else {
            $message = 'Bad Request';
        }

        return $message;
    }
}