<?php

namespace App\Http\Controllers;

use App\Core\Session;
use App\Http\BaseController;
use App\Http\RedirectResponse;
use App\Http\Request;
use App\Model\BattleshipGame;
use App\Model\GameMessages;

class GameController extends BaseController
{
    protected $gameInstance;

    public function __construct(Request $request, Session $session, BattleshipGame $gameInstance)
    {
        parent::__construct($request, $session);
        $this->gameInstance = $gameInstance;
    }

    public function renderAction()
    {
        $this->session->set('show', '');
        $this->gameInstance->saveGameState();

        return $this->render('game-board', [
            'game' => $this->gameInstance
        ]);
    }

    public function restartAction()
    {
        $this->gameInstance->restartGame();
        $this->gameInstance->saveGameState();

        return new RedirectResponse('/');
    }

    public function fireAction()
    {
        try {
            if (strtolower($this->request->getPost('coordinates')) == 'show') {
                $this->session->set('show', 'show');
            } else {
                list($x,$y) = $this->gameInstance->parseFireCoordinates($this->request->getPost('coordinates'));
                if($this->gameInstance->isHit($x, $y)) {
                    $this->gameInstance->registerHit($x, $y);
                    $this->setMessage(GameMessages::HIT);
                } else {
                    $this->setMessage(GameMessages::MISS);
                }
            }
        } catch (\Exception $e) {
            $this->setMessage(GameMessages::ERROR);
        }

        $this->countAttempts();
        $this->gameInstance->saveGameState();

        return $this->render('game-board', [
            'game' => $this->gameInstance
        ]);
    }

    private function setMessage(string $message) : void
    {
        $this->session->set('message', $message);
    }
    private function countAttempts()
    {
        $attempts = $this->session->get('attempts') != null ? (int) $this->session->get('attempts') : 0;
        $attempts++;
        $this->session->set('attempts', $attempts);
    }
}