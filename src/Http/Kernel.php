<?php
namespace App\Http;

use App\Core\Interfaces\ResponseInterface;
use App\Core\Router;

class Kernel
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Request $request, Router $router)
    {
        $this->request = $request;
        $this->router = $router;
    }

    public function handle(): ResponseInterface
    {
        // match request to a controller
        if ($this->router->match()) {
            // call controller and get response
            $response = $this->router->execute();
            if ($response instanceof ResponseInterface) {
                return $response;
            }
        }

        return new Response(404, 'The page cannot be found');
    }

    /**
     * @return Router
     */
    public function getRouter() : Router
    {
        return $this->router;
    }
}