<?php

namespace App\Http;

class Request
{
    const TYPE_GET  = 'GET';
    const TYPE_POST = 'POST';

    private $server;
    private $get;
    private $post;
    private $type;

    public function __construct()
    {
        if (php_sapi_name() === 'cli') {
            $this->type = 'cli';
        } else {
            $this->type = 'web';
        }

        $this->server = $_SERVER;
        $this->get = $_GET;
        $this->post = $_POST;
    }

    public function getUri()
    {
        return $this->getRequestUri();
    }

    public function isPost()
    {
        return strtoupper($this->server['REQUEST_METHOD']) == self::TYPE_POST;
    }

    public function isGet()
    {
        return strtoupper($this->server['REQUEST_METHOD']) == self::TYPE_GET;
    }

    public function getRequestUri()
    {
        return isset($this->server['REQUEST_URI'])
            ? $this->server['REQUEST_URI'] : '';
    }

    public function isWeb()
    {
        return !$this->isCli();
    }

    public function isCli()
    {
        return $this->type === 'cli';
    }

    public function getPost(string $key)
    {
        return isset($_POST[$key]) ? $_POST[$key] : null;
    }
}