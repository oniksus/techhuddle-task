<?php

namespace App\Http;

use App\Core\Interfaces\ResponseInterface;

class RedirectResponse implements ResponseInterface
{
    private $location;

    public function __construct(string $location) {
        $this->location = $location;
    }

    public function send() : ResponseInterface
    {
        $this->sendHeaders();

        return $this;
    }

    public function sendHeaders()
    {
        if (headers_sent()) {
            return $this;
        }

        header(sprintf("Location: %s", $this->location), true, 301);
        exit();
    }
}