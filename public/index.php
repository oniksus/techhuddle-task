<?php

use App\Http\Kernel as HttpKernel;
use App\Http\Request;

require_once __DIR__ . '/../src/bootstrap.php';

$request = getClass(Request::class);
if ($request->isWeb()) {
    /** @var HttpKernel $app */
    $app = getClass(HttpKernel::class);
    // Register rout to controller mapping
    $app->getRouter()
        ->get('/', 'GameController@render')
        ->post('/', 'GameController@fire')
        ->get('/restart', 'GameController@restart');

    // get response
    $response = $app->handle();
    $response->send();
} else {
    throwException('Needs to creat CLI kernel');
}