# Start server

php -S localhost:8000 ./public

# Run tests

 -> composer install to get PHPUnit
php vendor/bin/phpunit --configuration ./phpunit.xml
