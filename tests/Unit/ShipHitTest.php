<?php

namespace Tests\Unit;

use App\Model\Ships\Battleship;
use App\Model\Ships\Destroyer;
use PHPUnit\Framework\TestCase;

class ShipHitTest extends TestCase
{
    public function testHitShip()
    {
        $ship = new Battleship(1,1,5,1);
        $this->assertTrue($ship->isHit(3,1), 'Failed to hit Battleship 1');

        $ship = new Battleship(1,1,5,1);
        $this->assertTrue($ship->isHit(1,1), 'Failed to hit Battleship 2');

        $ship = new Battleship(1,1,5,1);
        $this->assertTrue($ship->isHit(5,1), 'Failed to hit Battleship 3');

        $ship = new Destroyer(1,1,4,1);
        $this->assertTrue($ship->isHit(3,1), 'Failed to hit Destroyer 4');
        $this->assertTrue(!$ship->isHit(5,1), 'Failed to miss Destroyer 4');
    }
}