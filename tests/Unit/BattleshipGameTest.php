<?php

namespace Tests\Unit;

use App\Http\Request;
use App\Model\BattleshipGame;
use App\Utils\TestSessionHelper;
use PHPUnit\Framework\TestCase;

class BattleshipGameTest extends TestCase
{
    private $fieldBoardV1 = ['1:3','2:3','3:3','4:3','5:2','5:3','5:4','5:5','6:1','6:2','6:3','6:4','6:5'];
    private $fieldBoardV2 = ['4:0','5:0', '6:0','7:0','5:3','5:4','5:5','5:6','5:7','1:8','2:8','3:8','4:8'];

    public function testInitGame()
    {
        $game = new BattleshipGame(new Request(), new TestSessionHelper([]));

        $game->resetGame();
        $this->assertInstanceOf(BattleshipGame::class, $game->initGame());
        $game->resetGame();
        $this->assertInstanceOf(BattleshipGame::class, $game->initGame());
        $game->resetGame();
        $this->assertInstanceOf(BattleshipGame::class, $game->initGame());
        $game->resetGame();
        $this->assertInstanceOf(BattleshipGame::class, $game->initGame());
    }

    public function testDrawableDirections()
    {
        $d = BattleshipGame::DRAWABLE_DOWN;
        $l = BattleshipGame::DRAWABLE_LEFT;
        $r = BattleshipGame::DRAWABLE_RIGHT;
        $u = BattleshipGame::DRAWABLE_UP;

        $game = new BattleshipGame(new Request(), new TestSessionHelper([]));

        $reflector = new \ReflectionObject($game);
        $method = $reflector->getMethod('getDrawableDirections');
        $method->setAccessible(true);

        $directions = $method->invoke($game, 1, 1, 5, []);
        $this->assertEquals([$d, $r], $directions, 'Get draw directions -> 1');

        $directions = $method->invoke($game, 8, 8, 5, []);
        $this->assertEquals([$l, $u], $directions, 'Get draw directions -> 2');

        $directions = $method->invoke($game, 1, 1, 5, $this->fieldBoardV1);
        $this->assertEquals([$r], $directions, 'Get draw directions -> 3');

        $directions = $method->invoke($game, 5, 0, 5, $this->fieldBoardV1);
        $this->assertEquals([$l,$r], $directions, 'Get draw directions -> 4');

        $directions = $method->invoke($game, 4, 3, 4, $this->fieldBoardV2);
        $this->assertEquals([$d, $l], $directions, 'Get draw directions -> 5');

        $directions = $method->invoke($game, 6, 7, 4, $this->fieldBoardV2);
        $this->assertEquals([$r,$u], $directions, 'Get draw directions -> 5');
    }
}