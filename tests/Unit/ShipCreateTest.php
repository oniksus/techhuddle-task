<?php

namespace Tests\Unit;

use App\Exception\InvalidShip;
use App\Model\Ships\Battleship;
use App\Model\Ships\Destroyer;
use PHPUnit\Framework\TestCase;

class ShipCreateTest extends TestCase
{
    public function testCreateBattleship()
    {
        $battleShipWasCreated = false;
        try {
            $ship = new Battleship(1,0,5,0);
            $battleShipWasCreated = true;
        } catch (\Exception $exception) {};
        $this->assertTrue($battleShipWasCreated, 'Failed to create Battleship 1');

        $battleShipWasCreated = false;
        try {
            $ship = new Battleship(3,3,3,7);
            $battleShipWasCreated = true;
        } catch (\Exception $exception) {};
        $this->assertTrue($battleShipWasCreated, 'Failed to create Battleship 2');
    }

    public function testShipCoordinates()
    {
        $ship = new Battleship(1,0,5,0);
        $this->assertEquals(array_values($ship->getShipCoordinates()), ['1:0','2:0','3:0','4:0','5:0'], "Invalid Coordinates for  Battleship 1");

        $ship = new Battleship(3,3,3,7);
        $this->assertEquals(array_values($ship->getShipCoordinates()), ['3:3','3:4','3:5','3:6','3:7'], "Invalid Coordinates for  Battleship 2");
    }

    public function testCreateDestroyer()
    {
        $created = false;
        try {
            new Destroyer(1,1,4,1);
            $created = true;
        } catch (\Exception $exception) {};

        $this->assertTrue($created, 'Failed to create Destroyer 1');

        $created = false;
        try {
            new Destroyer(1,1,1,4);
            $created = true;
        } catch (\Exception $exception) {};

        $this->assertTrue($created, 'Failed to create Destroyer 2');

        $created = false;
        try {
            new Destroyer(3,7,6,7);
            $created = true;
        } catch (\Exception $exception) {};

        $this->assertTrue($created, 'Failed to create Destroyer 3');

        $created = false;
        try {
            new Destroyer(9,8,9,5);
            $created = true;
        } catch (\Exception $exception) {};

        $this->assertTrue($created, 'Failed to create Destroyer 3');
    }

    public function testCreateShipExceptions1()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Destroyer(1,1,6,1);
    }

    public function testCreateShipExceptions2()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Destroyer(1,3,6,4);
    }

    public function testCreateShipExceptions3()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Battleship(1,3,6,4);
    }

    public function testCreateOutOdBoardShips()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Battleship(11,3,6,4);
    }

    public function testCreateOutOdBoardShips1()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Destroyer(1,1,-4,1);
    }

    public function testCreateOutOdBoardShips2()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Destroyer(-1,1,3,1);
    }

    public function testCreateOutOdBoardShips3()
    {
        $this->expectException(InvalidShip::class);
        $ship = new Destroyer(8,1,11,1);
    }
}